# 增强的命令行文本输出API for C/C++

[![pipeline status](https://gitlab.com/jiangzhiping/EnhancedConsoleText/badges/master/pipeline.svg)](https://gitlab.com/jiangzhiping/EnhancedConsoleText/commits/master)

## 背景与意义

对命令行文本输出指定字效以及颜色，可以**有效地提升基于CLI交互的美观以及注意力引导**。

相比一些已有的解决方案，本库同时具备如下几个亮点：
* 跨平台(Win/Unix/Mac)
* 支持Unicode文本（需要clang编译器)
* 简洁、直观的API
* 仅依赖ISO C++
* 优秀的文档
* MIT许可

## 基于CMake的安装方法

由于本库采用CMake管理，因此可以很方便地将本库加入其它由CMake管理的项目，操作共3步:
1. 通过“git submodule add”或直接下载的方式将本库加入到目标项目代码库中;
2. 在需要用到本库的头文件中加入 #include "../EnhancedConsoleText/src/EnhancedConsoleText.hxx" 或类似的头文件include，请确认路径是否正确;
3. 在调用本库的CMake目标(可执行或库)定义中，通过target_link_libraries加入本库的二进制依赖;


## 用法

本库的使用极为简便，只有一个核心方法(EnhancedConsoleText::println)。该方法的签名如下：
```
void println(const std::string &content,  // 要显示的内容
                 FontEffect fontEffect = FontEffect::Reset,  // 字体效果
                 LineAlignment lineAlignment = LineAlignment::Left, // 对齐方式
                 FrontColor frontColor = FrontColor::NoColor,  // 字体前景色
                 BackgroundColor bgColor = BackgroundColor::NoColor);  // 字体背景色
```
既对给定的字符串内容(content)依次指定字体效果(fontEffect), 行对齐(lineAlignment), 前景色(frontColor)以及背景色(bgColor)。
由于这四个参数都配置了默认值，因此在极简时，只需要EnhancedConsoleText::println(*要显示的内容string*)即可。

## 用例

1. 指定输出字体加粗
```
EnhancedConsoleText::println("指定这句话输出时采用字体加粗(Bold)", FontEffect::Bold);
```

2. 指定输出字体加下划线
```
EnhancedConsoleText::println("指定这句话输出时采用字体加下划线(Underline)", FontEffect::Underline);
```

3. 指定输出字体加下划线，并将此行对齐居中
```
EnhancedConsoleText::println("指定这句话输出时采用字体加下划线(Underline)并将文字对齐设置为居中(Center)", FontEffect::Underline, LineAlignment::Center);
```

4. 指定文字无字效、左对齐、颜色为红色
```
EnhancedConsoleText::println("指定这句话输出时采用字体无字效(Reset)、左对齐(Left)、前景色为红色(Red)", FontEffect::Reset, LineAlignment::Left, FrontColor::Red);
```

5. 指定文字加粗、居中、颜色为红色、背景为黄色
```
EnhancedConsoleText::println("指定这句话输出时采用字体加粗(Bold)、居中(Center)、前景色为红色(Red)，背景色为黄色(Yellow)", FontEffect::Bold, LineAlignment::Center, FrontColor::Red, BackgroundColor::Yellow);
```

## 手册

### 支持的字效
出于跨平台的原因，目前仅支持**三种字效**：
* 无字效(Reset)
* 加粗(Bold)
* 下划线(Underline)

## 支持的对齐
**EnhancedConsoleText::lineWidth** 控制着一行的宽度，默认为80。
基于lineWidth值，EnhancedConsoleText支持有三种对齐方式：
* 左对齐(Left)
* 居中(Center)
* 右对齐(Right)

## 支持的颜色
前景色与背景色支持相同的颜色值：
* 不指定颜色(NoColor)
* 黑色(Black)
* 红色(Red)
* 绿色(Green)
* 黄色(Yellow)
* 蓝色(Blue)
* 洋红色(Magenta)
* 青色(Cyan)
* 白色(White)