//
// Created by 蒋志平 on 2018/4/20.
//

#include "EnhancedConsoleText.hxx"

namespace EnhancedConsoleText {

    std::string stringSpace(int spaceCount) {
        std::string spaceString = "";
        for (int i = 0; i < spaceCount; ++i) {
            spaceString += " ";
        }

        return spaceString;
    }

    std::string decorateString(const std::string &content, FontEffect fontEffect, LineAlignment lineAlignment, FrontColor frontColor,
                               BackgroundColor bgColor) {
        auto wContent = string2WString(content);
        float wLength = wContent.size();
        wLength *= cjkSpaceRatio;

        auto resultingString = content;

        switch (lineAlignment) {
            case LineAlignment::Left : {
                int paddingSpaces_n = (lineWidth - wLength);
                auto paddingSpaces = stringSpace(paddingSpaces_n);
                resultingString = resultingString + paddingSpaces;
                break;
            }

            case LineAlignment::Center : {
                int paddingSpaces_n = (lineWidth - wLength) / 2;
                auto paddingSpaces = stringSpace(paddingSpaces_n);
                resultingString = paddingSpaces + resultingString + paddingSpaces;
                break;
            }

            case LineAlignment::Right : {
                int paddingSpaces_n = (lineWidth - wLength);
                auto paddingSpaces = stringSpace(paddingSpaces_n);
                resultingString = paddingSpaces + resultingString;
                break;
            }
        }

        return resultingString;
    }
}
