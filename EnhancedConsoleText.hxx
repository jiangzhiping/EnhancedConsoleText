//
// Created by 蒋志平 on 2018/4/19.
//

#ifndef ENHANCEDCONSOLETEXT_HXX
#define ENHANCEDCONSOLETEXT_HXX

#include <iostream>
#include <string>
#include <random>
#include <locale>
#include <codecvt>
#include <regex>
#include <vector>

#if defined (Q_OS_ANDROID)
#include <QDebug>
#include <QString>
#endif

enum class FrontColor {
    NoColor = 0,
    Black = 30,
    Red = 31,
    Green = 32,
    Yellow = 33,
    Blue = 34,
    Magenta = 35,
    Cyan = 36,
    While = 37,
};

enum class BackgroundColor {
    NoColor = 0,
    Black = 40,
    Red = 41,
    Green = 42,
    Yellow = 43,
    Blue = 44,
    Magenta = 45,
    Cyan = 46,
    While = 47,
};

enum class FontEffect : int {
    Reset = 0,
    Bold = 1,
    Underline = 4,
};

enum class LineAlignment {
    Left,
    Center,
    Right,
};

template<typename Enumeration>
constexpr std::enable_if_t<std::is_enum<Enumeration>::value,
        std::underlying_type_t<Enumeration>> as_integer(const Enumeration value) {
    return static_cast<std::underlying_type_t<Enumeration>>(value);
}

namespace EnhancedConsoleText {

    extern int lineWidth;
    extern float cjkSpaceRatio;

    std::wstring string2WString(std::string content);

    std::string wstring2String(std::wstring content);

    std::string decorateString(const std::string &content, FontEffect fontEffect, LineAlignment lineAlignment, FrontColor frontColor,
                               BackgroundColor bgColor);

    void print(const std::string &content, FontEffect frontEffect = FontEffect::Reset,
               LineAlignment lineAlignment = LineAlignment::Left,
               FrontColor frontColor = FrontColor::NoColor,
               BackgroundColor bgColor = BackgroundColor::NoColor);

    void println(const std::string &content, FontEffect frontEffect = FontEffect::Reset,
                 LineAlignment lineAlignment = LineAlignment::Left,
                 FrontColor frontColor = FrontColor::NoColor,
                 BackgroundColor bgColor = BackgroundColor::NoColor);

    void press2Continue(const std::string &content = "Press any key to continue...");
};

#define showAndRunCode(code) EnhancedConsoleText::println("[Code, line " + std::to_string(__LINE__) + ", method: "+__func__+"]=> \""+ #code +"\"", FontEffect::Reset, LineAlignment::Left, FrontColor::Green); code

#endif //ENHANCEDCONSOLETEXT_HXX
