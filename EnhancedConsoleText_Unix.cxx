//
// Created by 蒋志平 on 2018/4/20.
//

#include "EnhancedConsoleText.hxx"

namespace EnhancedConsoleText {

    std::pair<std::string, std::string> fontEnhancement(FontEffect fontEffect, FrontColor frontColor, BackgroundColor bgColor) {
        constexpr auto heading = "\033[";
        constexpr auto ending = "m";

        std::string beginningDeco = "";
        std::string endingDeco = "0";

        switch(fontEffect) {
            case FontEffect::Bold: {
                beginningDeco += "1;";
                break;
            }
            case FontEffect::Underline: {
                beginningDeco += "4;";
                break;
            }

            default: {
                beginningDeco += "0;";
                break;
            }
        }

        if (frontColor != FrontColor::NoColor) {
            beginningDeco += std::to_string(static_cast<int>(frontColor));
            beginningDeco += ";";
        }
        if (bgColor != BackgroundColor::NoColor) {
            beginningDeco += std::to_string(static_cast<int>(bgColor));
            beginningDeco += ";";
        }

        beginningDeco = std::regex_replace(beginningDeco, std::regex(";$"), ""); // remove the ending ";"
        return std::make_pair(heading+beginningDeco+ending, heading+endingDeco+ending);
    };

    std::string stringSpace(int spaceCount) {
        std::string spaceString = "";
        for (int i = 0; i < spaceCount; ++i) {
            spaceString += " ";
        }

        return spaceString;
    }

    std::string decorateString(const std::string &content, FontEffect fontEffect, LineAlignment lineAlignment, FrontColor frontColor,
                               BackgroundColor bgColor) {
        auto wContent = string2WString(content);
        float wLength = wContent.size();
        wLength *= cjkSpaceRatio;

        auto enhancingPrefixAndPostfix = fontEnhancement(fontEffect, frontColor, bgColor);
        auto resultingString = enhancingPrefixAndPostfix.first + content + enhancingPrefixAndPostfix.second;

        switch(lineAlignment) {
            case LineAlignment::Left : {
                int paddingSpaces_n = (lineWidth - wLength);
                auto paddingSpaces = stringSpace(paddingSpaces_n);
                resultingString = resultingString + paddingSpaces;
                break;
            }

            case LineAlignment::Center : {
                int paddingSpaces_n = (lineWidth - wLength)/2;
                auto paddingSpaces = stringSpace(paddingSpaces_n);
                resultingString = paddingSpaces + resultingString + paddingSpaces;
                break;
            }

            case LineAlignment::Right : {
                int paddingSpaces_n = (lineWidth - wLength);
                auto paddingSpaces = stringSpace(paddingSpaces_n);
                resultingString = paddingSpaces + resultingString;
                break;
            }
        }

        return resultingString;
    }
}
