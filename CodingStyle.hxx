//
// Created by csi on 11/24/18.
//

#ifndef ENHANCEDCONSOLETEXT_CODINGSTYLE_HXX
#define ENHANCEDCONSOLETEXT_CODINGSTYLE_HXX

#include "EnhancedConsoleText.hxx"

namespace EnhancedConsoleText::CodingStyle {

    inline void normalTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Reset, LineAlignment::Left, FrontColor::NoColor);
    }

    inline void blueNormalTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Reset, LineAlignment::Left, FrontColor::Blue);
    }

    inline void greenNormalTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Reset, LineAlignment::Left, FrontColor::Green);
    }

    inline void yellowNormalTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Reset, LineAlignment::Left, FrontColor::Yellow);
    }

    inline void redNormalTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Reset, LineAlignment::Left, FrontColor::Red);
    }

    inline void cyanNormalTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Reset, LineAlignment::Left, FrontColor::Cyan);
    }

    inline void magentaNormalTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Reset, LineAlignment::Left, FrontColor::Magenta);
    }

    inline void normalBoldTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Bold, LineAlignment::Left, FrontColor::NoColor);
    }

    inline void blueBoldTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Bold, LineAlignment::Left, FrontColor::Blue);
    }

    inline void greenBoldTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Bold, LineAlignment::Left, FrontColor::Green);
    }

    inline void yellowBoldTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Bold, LineAlignment::Left, FrontColor::Yellow);
    }

    inline void redBoldTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Bold, LineAlignment::Left, FrontColor::Red);
    }

    inline void cyanBoldTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Bold, LineAlignment::Left, FrontColor::Cyan);
    }

    inline void magentaBoldTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Bold, LineAlignment::Left, FrontColor::Magenta);
    }

    inline void normalCenteredTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Reset, LineAlignment::Center, FrontColor::NoColor);
    }

    inline void blueCenteredTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Reset, LineAlignment::Center, FrontColor::Blue);
    }

    inline void greenCenteredTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Reset, LineAlignment::Center, FrontColor::Green);
    }

    inline void yellowCenteredTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Reset, LineAlignment::Center, FrontColor::Yellow);
    }

    inline void redCenteredTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Reset, LineAlignment::Center, FrontColor::Red);
    }

    inline void cyanCenteredTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Reset, LineAlignment::Center, FrontColor::Cyan);
    }

    inline void magentaCenteredTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Reset, LineAlignment::Center, FrontColor::Magenta);
    }

    inline void normalCenteredBoldTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Bold, LineAlignment::Center, FrontColor::NoColor);
    }

    inline void blueCenteredBoldTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Bold, LineAlignment::Center, FrontColor::Blue);
    }

    inline void greenCenteredBoldTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Bold, LineAlignment::Center, FrontColor::Green);
    }

    inline void yellowCenteredBoldTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Bold, LineAlignment::Center, FrontColor::Yellow);
    }

    inline void redCenteredBoldTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Bold, LineAlignment::Center, FrontColor::Red);
    }

    inline void cyanCenteredBoldTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Bold, LineAlignment::Center, FrontColor::Cyan);
    }

    inline void magentaCenteredBoldTextPrint(const std::string &content) {
        EnhancedConsoleText::print(content, FontEffect::Bold, LineAlignment::Center, FrontColor::Magenta);
    }

    inline void normalTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Reset, LineAlignment::Left, FrontColor::NoColor);
    }

    inline void blueNormalTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Reset, LineAlignment::Left, FrontColor::Blue);
    }

    inline void greenNormalTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Reset, LineAlignment::Left, FrontColor::Green);
    }

    inline void yellowNormalTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Reset, LineAlignment::Left, FrontColor::Yellow);
    }

    inline void redNormalTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Reset, LineAlignment::Left, FrontColor::Red);
    }

    inline void cyanNormalTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Reset, LineAlignment::Left, FrontColor::Cyan);
    }

    inline void magentaNormalTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Reset, LineAlignment::Left, FrontColor::Magenta);
    }

    inline void normalBoldTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Bold, LineAlignment::Left, FrontColor::NoColor);
    }

    inline void blueBoldTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Bold, LineAlignment::Left, FrontColor::Blue);
    }

    inline void greenBoldTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Bold, LineAlignment::Left, FrontColor::Green);
    }

    inline void yellowBoldTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Bold, LineAlignment::Left, FrontColor::Yellow);
    }

    inline void redBoldTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Bold, LineAlignment::Left, FrontColor::Red);
    }

    inline void cyanBoldTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Bold, LineAlignment::Left, FrontColor::Cyan);
    }

    inline void magentaBoldTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Bold, LineAlignment::Left, FrontColor::Magenta);
    }

    inline void normalCenteredTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Reset, LineAlignment::Center, FrontColor::NoColor);
    }

    inline void blueCenteredTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Reset, LineAlignment::Center, FrontColor::Blue);
    }

    inline void greenCenteredTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Reset, LineAlignment::Center, FrontColor::Green);
    }

    inline void yellowCenteredTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Reset, LineAlignment::Center, FrontColor::Yellow);
    }

    inline void redCenteredTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Reset, LineAlignment::Center, FrontColor::Red);
    }

    inline void cyanCenteredTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Reset, LineAlignment::Center, FrontColor::Cyan);
    }

    inline void magentaCenteredTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Reset, LineAlignment::Center, FrontColor::Magenta);
    }

    inline void normalCenteredBoldTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Bold, LineAlignment::Center, FrontColor::NoColor);
    }

    inline void blueCenteredBoldTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Bold, LineAlignment::Center, FrontColor::Blue);
    }

    inline void greenCenteredBoldTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Bold, LineAlignment::Center, FrontColor::Green);
    }

    inline void yellowCenteredBoldTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Bold, LineAlignment::Center, FrontColor::Yellow);
    }

    inline void redCenteredBoldTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Bold, LineAlignment::Center, FrontColor::Red);
    }

    inline void cyanCenteredBoldTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Bold, LineAlignment::Center, FrontColor::Cyan);
    }

    inline void magentaCenteredBoldTextPrintln(const std::string &content) {
        EnhancedConsoleText::println(content, FontEffect::Bold, LineAlignment::Center, FrontColor::Magenta);
    }
}

#endif //ENHANCEDCONSOLETEXT_CODINGSTYLE_HXX
