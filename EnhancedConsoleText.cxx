//
// Created by 蒋志平 on 2018/4/19.
//

#include "EnhancedConsoleText.hxx"

namespace EnhancedConsoleText {
    int lineWidth = 80;
    float cjkSpaceRatio = 1.5;

    std::wstring string2WString(std::string content) {
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
        return converter.from_bytes(content);
    }

    std::string wstring2String(std::wstring content) {
        std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
        return converter.to_bytes(content);
    }

    void print(const std::string &content, FontEffect fontEffect, LineAlignment lineAlignment, FrontColor frontColor, BackgroundColor bgColor) {
        std::regex newlineRegex("\n");
        std::smatch match;

        std::vector<std::string> lineSplits{
                std::sregex_token_iterator(content.begin(), content.end(), newlineRegex, -1), {}
        };

        for (auto i = 0; i < lineSplits.size(); i++) {
#if defined (Q_OS_ANDROID)
            qDebug() << decorateString(lineSplits[i], fontEffect, lineAlignment, frontColor, bgColor) << std::endl;
#else
            std::cout << decorateString(lineSplits[i], fontEffect, lineAlignment, frontColor, bgColor) << std::endl;
#endif
        }

    }

    void println(const std::string &content, FontEffect fontEffect, LineAlignment lineAlignment, FrontColor frontColor,
                 BackgroundColor bgColor) {
        std::regex newlineRegex("\n");
        std::regex headingSpaceRegex("^\\s*");
        std::regex tailingSpaceRegex("\\s*$");
        std::smatch match;

        auto headingNewlines = 0;
        auto trailingNewlines = 0;

        std::string spaceTrimedContent;

        try {
            if (std::regex_search(content, match, headingSpaceRegex)) {
                auto headingSpaceStr = match.str();
                headingNewlines = headingSpaceStr.size();
            }

            if (std::regex_search(content, match, tailingSpaceRegex) && match.size() > 0) {
                auto trailingSpaceStr = match.str(0);
                trailingNewlines = trailingSpaceStr.size();
            }

            spaceTrimedContent = std::regex_replace(content, headingSpaceRegex, "");
            spaceTrimedContent = std::regex_replace(spaceTrimedContent, tailingSpaceRegex, "");

        } catch (std::regex_error &e) {
            // Syntax error in the regular expression
#if defined (Q_OS_ANDROID)
            qDebug() << e.what() << std::endl;
#else
            std::cout << e.what() << std::endl;
#endif
        }

        std::vector<std::string> lineSplits{
                std::sregex_token_iterator(spaceTrimedContent.begin(), spaceTrimedContent.end(), newlineRegex, -1), {}
        };

        for (auto i = 0; i < headingNewlines; i++) {
#if defined (Q_OS_ANDROID)
            qDebug() << std::endl;
#else
            std::cout << std::endl;
#endif
        }


        for (auto i = 0; i < lineSplits.size(); i++) {
#if defined (Q_OS_ANDROID)
            qDebug() << decorateString(lineSplits[i], fontEffect, lineAlignment, frontColor, bgColor) << std::endl;
#else
            std::cout << decorateString(lineSplits[i], fontEffect, lineAlignment, frontColor, bgColor) << std::endl;
#endif
        }

        for (auto i = 0; i < trailingNewlines; i++) {
#if defined (Q_OS_ANDROID)
            qDebug() << std::endl;
#else
            std::cout << std::endl;
#endif
        }

    }

    void press2Continue(const std::string &content) {
        println(content);
        // very ugly way to suppress warning
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
        system("/bin/bash -c \"read -n 1 -s\"");
#pragma GCC diagnostic pop
    }
}