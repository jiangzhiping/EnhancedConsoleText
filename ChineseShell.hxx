//
// Created by 蒋志平 on 2018/4/20.
//

#ifndef CHINESESHELL_HXX
#define CHINESESHELL_HXX

#include "EnhancedConsoleText.hxx"

typedef FontEffect 字效;
typedef LineAlignment 对齐方式;
typedef FrontColor 前景色;
typedef BackgroundColor 背景色;

#define 加粗 FontEffect::Bold
#define 下划线 FontEffect::Underline
#define 正常 FontEffect::Reset

#define 红色 FrontColor::Red
#define 黑色 FrontColor::Black
#define 蓝色 FrontColor::Blue
#define 绿色 FrontColor::Green
#define 黄色 FrontColor::Yellow
#define 洋红色 FrontColor::Magenta
#define 青色 FrontColor::Cyan
#define 白色 FrontColor::White
#define 无色 FrontColor::NoColor

#define 红色背景 BackgroundColor::Red
#define 黑色背景 BackgroundColor::Black
#define 蓝色背景 BackgroundColor::Blue
#define 绿色背景 BackgroundColor::Green
#define 黄色背景 BackgroundColor::Yellow
#define 洋红色背景 BackgroundColor::Magenta
#define 青色背景 BackgroundColor::Cyan
#define 白色背景 BackgroundColor::White
#define 无色背景 BackgroundColor::NoColor

#define 左对齐 LineAlignment::Left
#define 居中 LineAlignment::Center
#define 右对齐 LineAlignment::Right

inline void 显示(const std::string &内容, 字效 fontEffect = 正常,
        对齐方式 lineAlignment = 左对齐,
        前景色 frontColor = 无色,
        背景色 bgColor    = 无色背景) {
    EnhancedConsoleText::println(内容, fontEffect, lineAlignment, frontColor, bgColor);
}

inline void 暂停(const std::string &暂停时显示的内容) {
    EnhancedConsoleText::press2Continue(暂停时显示的内容);
}

#define 一级标题(content) 显示(content, 加粗, 居中, 红色)
#define 二级标题(content) 显示(content, 加粗, 居中, 无色)
#define 三级标题(content) 显示(content, 正常, 居中, 无色)
#define 小节标题(content) 显示(content, 加粗, 左对齐, 无色)

#define 正文(content) 显示(content, 正常, 左对齐, 无色)
#define 正文居中(content) 显示(content, 正常, 居中, 无色)
#define 加粗正文(content) 显示(content, 加粗, 左对齐, 无色)
#define 红色加粗正文(content) 显示(content, 加粗, 左对齐, 红色)
#define 绿色加粗正文(content) 显示(content, 加粗, 左对齐, 红色)

#define 显示并运行代码(code) 显示("[第" + std::to_string(__LINE__) + "行, 方法:"+__func__+"]=> \""+ #code +"\"", 正常, 左对齐, 绿色); code

#endif //CHINESESHELL_HXX
