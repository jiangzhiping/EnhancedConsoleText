//
// Created by csi on 11/24/18.
//

#include <iostream>
#include "CodingStyle.hxx"

int main() {
    EnhancedConsoleText::lineWidth = 120;
    EnhancedConsoleText::println("Bold Green Centered Title with Red background", FontEffect::Bold, LineAlignment::Center, FrontColor::Green, BackgroundColor::Red);


    EnhancedConsoleText::CodingStyle::normalTextPrint("EnhancedConsoleText::\nCodingStyle::\nnormalTextPrint\n");
    EnhancedConsoleText::CodingStyle::cyanNormalTextPrint("EnhancedConsoleText::CodingStyle::cyanNormalTextPrint\n");
    EnhancedConsoleText::CodingStyle::greenNormalTextPrint("EnhancedConsoleText::CodingStyle::greenNormalTextPrint\n");
    EnhancedConsoleText::CodingStyle::redNormalTextPrint("EnhancedConsoleText::CodingStyle::redNormalTextPrint\n");
    EnhancedConsoleText::CodingStyle::yellowNormalTextPrint("EnhancedConsoleText::CodingStyle::yellowNormalTextPrint\n");
    EnhancedConsoleText::CodingStyle::magentaNormalTextPrint("EnhancedConsoleText::CodingStyle::magentaNormalTextPrint\n");
    EnhancedConsoleText::CodingStyle::blueNormalTextPrint("EnhancedConsoleText::CodingStyle::blueNormalTextPrint\n");

    EnhancedConsoleText::CodingStyle::normalBoldTextPrint("EnhancedConsoleText::CodingStyle::normalBoldTextPrint\n");
    EnhancedConsoleText::CodingStyle::cyanBoldTextPrint("EnhancedConsoleText::CodingStyle::cyanBoldTextPrint\n");
    EnhancedConsoleText::CodingStyle::greenBoldTextPrint("EnhancedConsoleText::\nCodingStyle::\nnormalTextPrint\n");
    EnhancedConsoleText::CodingStyle::redBoldTextPrint("EnhancedConsoleText::CodingStyle::redBoldTextPrint\n");
    EnhancedConsoleText::CodingStyle::yellowBoldTextPrint("EnhancedConsoleText::CodingStyle::yellowBoldTextPrint\n");
    EnhancedConsoleText::CodingStyle::magentaBoldTextPrint("EnhancedConsoleText::CodingStyle::magentaBoldTextPrint\n");
    EnhancedConsoleText::CodingStyle::blueBoldTextPrint("EnhancedConsoleText::CodingStyle::blueBoldTextPrint\n");

    EnhancedConsoleText::CodingStyle::normalCenteredTextPrint("EnhancedConsoleText::CodingStyle::normalCenteredTextPrint\n");
    EnhancedConsoleText::CodingStyle::cyanCenteredTextPrint("EnhancedConsoleText::CodingStyle::cyanCenteredTextPrint\n");
    EnhancedConsoleText::CodingStyle::greenCenteredTextPrint("EnhancedConsoleText::CodingStyle::greenCenteredTextPrint\n");
    EnhancedConsoleText::CodingStyle::redCenteredTextPrint("EnhancedConsoleText::CodingStyle::redCenteredTextPrint\n");
    EnhancedConsoleText::CodingStyle::yellowCenteredTextPrint("EnhancedConsoleText::CodingStyle::yellowCenteredTextPrint\n");
    EnhancedConsoleText::CodingStyle::magentaCenteredTextPrint("EnhancedConsoleText::CodingStyle::magentaCenteredTextPrint\n");
    EnhancedConsoleText::CodingStyle::blueCenteredTextPrint("EnhancedConsoleText::CodingStyle::blueCenteredTextPrint\n");

    EnhancedConsoleText::CodingStyle::normalCenteredBoldTextPrint("EnhancedConsoleText::CodingStyle::normalCenteredBoldTextPrint\n");
    EnhancedConsoleText::CodingStyle::cyanCenteredBoldTextPrint("EnhancedConsoleText::CodingStyle::cyanCenteredBoldTextPrint\n");
    EnhancedConsoleText::CodingStyle::greenCenteredBoldTextPrint("EnhancedConsoleText::CodingStyle::greenCenteredBoldTextPrint\n");
    EnhancedConsoleText::CodingStyle::redCenteredBoldTextPrint("EnhancedConsoleText::CodingStyle::redCenteredBoldTextPrint\n");
    EnhancedConsoleText::CodingStyle::yellowCenteredBoldTextPrint("EnhancedConsoleText::CodingStyle::yellowCenteredBoldTextPrint\n");
    EnhancedConsoleText::CodingStyle::magentaCenteredBoldTextPrint("EnhancedConsoleText::CodingStyle::magentaCenteredBoldTextPrint\n");
    EnhancedConsoleText::CodingStyle::blueCenteredBoldTextPrint("EnhancedConsoleText::CodingStyle::blueCenteredBoldTextPrint\n");


    EnhancedConsoleText::CodingStyle::normalTextPrintln("EnhancedConsoleText::\nCodingStyle::\nnormalTextPrintln");
    EnhancedConsoleText::CodingStyle::cyanNormalTextPrintln("EnhancedConsoleText::CodingStyle::cyanNormalTextPrintln");
    EnhancedConsoleText::CodingStyle::greenNormalTextPrintln("EnhancedConsoleText::CodingStyle::greenNormalTextPrintln");
    EnhancedConsoleText::CodingStyle::redNormalTextPrintln("EnhancedConsoleText::CodingStyle::redNormalTextPrintln");
    EnhancedConsoleText::CodingStyle::yellowNormalTextPrintln("EnhancedConsoleText::CodingStyle::yellowNormalTextPrintln");
    EnhancedConsoleText::CodingStyle::magentaNormalTextPrintln("EnhancedConsoleText::CodingStyle::magentaNormalTextPrintln");
    EnhancedConsoleText::CodingStyle::blueNormalTextPrintln("EnhancedConsoleText::CodingStyle::blueNormalTextPrintln");

    EnhancedConsoleText::CodingStyle::normalBoldTextPrintln("EnhancedConsoleText::CodingStyle::normalBoldTextPrintln");
    EnhancedConsoleText::CodingStyle::cyanBoldTextPrintln("EnhancedConsoleText::CodingStyle::cyanBoldTextPrintln");
    EnhancedConsoleText::CodingStyle::greenBoldTextPrintln("EnhancedConsoleText::\nCodingStyle::\nnormalTextPrintln");
    EnhancedConsoleText::CodingStyle::redBoldTextPrintln("EnhancedConsoleText::CodingStyle::redBoldTextPrintln");
    EnhancedConsoleText::CodingStyle::yellowBoldTextPrintln("EnhancedConsoleText::CodingStyle::yellowBoldTextPrintln");
    EnhancedConsoleText::CodingStyle::magentaBoldTextPrintln("EnhancedConsoleText::CodingStyle::magentaBoldTextPrintln");
    EnhancedConsoleText::CodingStyle::blueBoldTextPrintln("EnhancedConsoleText::CodingStyle::blueBoldTextPrintln");

    EnhancedConsoleText::CodingStyle::normalCenteredTextPrintln("EnhancedConsoleText::CodingStyle::normalCenteredTextPrintln");
    EnhancedConsoleText::CodingStyle::cyanCenteredTextPrintln("EnhancedConsoleText::CodingStyle::cyanCenteredTextPrintln");
    EnhancedConsoleText::CodingStyle::greenCenteredTextPrintln("EnhancedConsoleText::CodingStyle::greenCenteredTextPrintln");
    EnhancedConsoleText::CodingStyle::redCenteredTextPrintln("EnhancedConsoleText::CodingStyle::redCenteredTextPrintln");
    EnhancedConsoleText::CodingStyle::yellowCenteredTextPrintln("EnhancedConsoleText::CodingStyle::yellowCenteredTextPrintln");
    EnhancedConsoleText::CodingStyle::magentaCenteredTextPrintln("EnhancedConsoleText::CodingStyle::magentaCenteredTextPrintln");
    EnhancedConsoleText::CodingStyle::blueCenteredTextPrintln("EnhancedConsoleText::CodingStyle::blueCenteredTextPrintln");

    EnhancedConsoleText::CodingStyle::normalCenteredBoldTextPrintln("EnhancedConsoleText::CodingStyle::normalCenteredBoldTextPrintln");
    EnhancedConsoleText::CodingStyle::cyanCenteredBoldTextPrintln("EnhancedConsoleText::CodingStyle::cyanCenteredBoldTextPrintln");
    EnhancedConsoleText::CodingStyle::greenCenteredBoldTextPrintln("EnhancedConsoleText::CodingStyle::greenCenteredBoldTextPrintln");
    EnhancedConsoleText::CodingStyle::redCenteredBoldTextPrintln("EnhancedConsoleText::CodingStyle::redCenteredBoldTextPrintln");
    EnhancedConsoleText::CodingStyle::yellowCenteredBoldTextPrintln("EnhancedConsoleText::CodingStyle::yellowCenteredBoldTextPrintln");
    EnhancedConsoleText::CodingStyle::magentaCenteredBoldTextPrintln("EnhancedConsoleText::CodingStyle::magentaCenteredBoldTextPrintln");
    EnhancedConsoleText::CodingStyle::blueCenteredBoldTextPrintln("EnhancedConsoleText::CodingStyle::blueCenteredBoldTextPrintln");

    return 0;
}